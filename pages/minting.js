import { useState, useEffect } from "react";
import Image from "next/image";

import nft from "../public/images/nft.png";
import opensea from "../public/images/opensealogo.png";
import twitter from "../public/images/twitterlogo.png";
import Logo from "../public/images/Logo_BB_Black.png";
import Link from "next/link";
import LoaderSpinner from "../components/loaderSpinner/loaderSpinner";

export default function Minting() {
  const [offset, setOffset] = useState(0);
  const [count, setCount] = useState(0);
  const [userWallet, setUserWallet] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    // clean up code
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  const addMint = () => {
    setCount((prevCount) => prevCount + 1);
  };

  const removeMint = () => {
    if (count == 0) {
      return;
    }

    setCount((prevCount) => prevCount - 1);
  };

  return (
    <div>
      {/* Header Start */}
      <header
        className={`bg-transparent absolute top-0 left-0 w-full flex items-center z-10 ${
          offset > 0 && "navbar-fixed"
        }`}
      >
        <div className="container mx-auto lg:flex lg:items-center lg:justify-center">
          <div className="flex items-center justify-between relative lg:w-3/4">
            <div className="px-2 sm:px-4">
              <Link href="/">
                <a className="font-bold text-lg text-primary py-6 flex items-center space-x-2">
                  <div className="w-[36px] h-[36px] relative">
                    <Image
                      alt="logo"
                      src={Logo}
                      layout="fill"
                      objectFit="cover"
                    />
                  </div>
                  <div className="hidden md:block mt-2">BeaconBeings</div>
                </a>
              </Link>
            </div>
            <div className="flex flex-wrap items-center px-2 sm:px-4">
              <nav
                className={`py-5 w-full right-4 top-full lg:block lg:static bg-transparent`}
              >
                <ul className="flex items-center">
                  <li className="group">
                    <a
                      href="#"
                      className="mx-2 sm:mx-4 flex items-center justify-center w-[32px] h-[32px]"
                    >
                      <div className="w-full h-full relative">
                        <Image
                          alt="image"
                          src={twitter}
                          layout="fill"
                          objectFit="cover"
                        />
                      </div>
                    </a>
                  </li>
                  <li className="group">
                    <a
                      href="#"
                      className="mx-2 sm:mx-4 flex items-center justify-center w-[24px] h-[24px]"
                    >
                      <div className="w-full h-full relative">
                        <Image
                          alt="image"
                          src={opensea}
                          layout="fill"
                          objectFit="cover"
                        />
                      </div>
                    </a>
                  </li>
                  <div
                    onClick={() => {
                      setUserWallet("1ExAmpLe0FaBiTco1NADr3sSV5tsGaMF6hd");
                    }}
                    className="mx-1 sm:mx-4 max-w-[150px] lg:max-w-[250px] py-2 rounded-full text-center bg-primary text-white cursor-pointer"
                  >
                    <p className="mx-4 truncate">
                      {userWallet ? userWallet : "Connect To Wallet"}
                    </p>
                  </div>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>
      {/* Header End */}

      <section id="minting" className="pt-36">
        <div className="container mx-auto">
          <div className="flex items-center justify-center flex-wrap">
            <div className="w-full flex items-center justify-center self-center px-4 lg:w-3/4 card">
              {!userWallet ? (
                <div
                  onClick={() => {
                    setUserWallet("1ExAmpLe0FaBiTco1NADr3sSV5tsGaMF6hd");
                  }}
                  className="mx-4 w-[250px] py-2 rounded-full text-center bg-primary text-white cursor-pointer"
                >
                  <p className="mx-4 truncate">
                    {userWallet ? userWallet : "Connect To Wallet"}
                  </p>
                </div>
              ) : (
                <div className="flex flex-col items-center gap-4 shadow-2xl rounded-xl px-8 py-12 text-lg  mt-8 lg:w-3/4">
                  <div className="font-bold text-2xl">Beacon Beings NFT</div>
                  <div className="font-semibold text-xl">Public Mint</div>
                  <div className="text-lg">6 / 50</div>
                  <div className="flex items-center gap-4">
                    <div
                      onClick={removeMint}
                      className="text-lg w-[28px] h-[28px] rounded-full flex items-center justify-center bg-primary text-white cursor-pointer"
                    >
                      -
                    </div>
                    <div className="text-lg">{count}</div>
                    <div
                      onClick={addMint}
                      className="text-lg w-[28px] h-[28px] rounded-full flex items-center justify-center bg-primary text-white cursor-pointer"
                    >
                      +
                    </div>
                  </div>
                  <div className="text-lg">{count * 0.00043} ETH</div>
                  <div onClick={() => setIsLoading(prevState => !prevState)} className="group px-6 py-2 w-[120px] flex items-center justify-center bg-primary border border-primary rounded-full text-white hover:text-primary hover:bg-white transition ease-in-out duration-300 text-base font-semibold cursor-pointer">
                    {isLoading ? <LoaderSpinner /> : "MINT"}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
