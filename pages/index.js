import { useState, useEffect } from "react";
import Image from "next/image";

import nft from "../public/images/nft.png";
import opensea from "../public/images/opensealogo.png";
import twitter from "../public/images/twitterlogo.png";
import Logo from "../public/images/Logo_BB_Black.png";
import Link from "next/link";

export default function Home() {
  const [isHamburgerActive, setIsHamburgerActive] = useState(false);
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    // clean up code
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <div>
      {/* Header Start */}
      <header
        className={`bg-transparent absolute top-0 left-0 w-full flex items-center z-10 ${
          offset > 0 && "navbar-fixed"
        }`}
      >
        <div className="container mx-auto lg:flex lg:items-center lg:justify-center">
          <div className="flex items-center justify-between relative lg:w-3/4">
            <div className="px-4">
              <a
                href="#home"
                className="font-bold text-lg text-primary py-6 flex items-center space-x-2"
              >
                <div className="w-[36px] h-[36px] relative">
                  <Image
                    alt="logo"
                    src={Logo}
                    layout="fill"
                    objectFit="cover"
                  />
                </div>
                <div className="hidden md:block mt-2">BeaconBeings</div>
              </a>
            </div>
            <div className="flex items-center px-4">
              <nav
                className={`w-full right-4 top-full lg:block lg:static bg-transparent`}
              >
                <ul className="flex items-center">
                  <li className="group">
                    <a
                      href="#"
                      className="mx-4 sm:mx-4 flex items-center justify-center w-[32px] h-[32px]"
                    >
                      <div className="w-full h-full relative">
                        <Image
                          alt="twitterlogo"
                          src={twitter}
                          layout="fill"
                          objectFit="cover"
                        />
                      </div>
                    </a>
                  </li>
                  <li className="group">
                    <a
                      href="#"
                      className="mx-4 sm:mx-4 flex items-center justify-center w-[24px] h-[24px]"
                    >
                      <div className="w-full h-full relative">
                        <Image
                          alt="opensealogo"
                          src={opensea}
                          layout="fill"
                          objectFit="cover"
                        />
                      </div>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>
      {/* Header End */}

      {/* Hero Section Start */}
      <section id="home" className="pt-36">
        <div className="container mx-auto">
          <div className="flex items-center justify-center flex-wrap">
            <div className="w-full self-center px-4 lg:w-3/4">
              <a
                href="#"
                className="text-base font-semibold text-white bg-primary py-1 px-4 rounded-full hover:shadow-lg hover:opacity-80 transition duration-300 ease-in-out"
              >
                blockchain
              </a>
              <h1 className="block font-bold text-dark text-4xl mt-12 lg:text-5xl">
                Beacon Beings
              </h1>
              <p className="font-medium text-secondary leading-relaxed text-lg  mt-8 lg:w-3/4">
                10,000 unique collectible characters with proof of ownership
                stored on the Ethereum blockchain. The project that inspired the
                modern CryptoArt movement. Selected press and appearances
                include Mashable, CNBC, The Financial Times, Bloomberg,
                MarketWatch, The Paris Review, Salon, The Outline, BreakerMag,
                Christie&apos;s of London, Art|Basel, The PBS NewsHour, The New
                York Times in 2018 and again in 2021. The Cryptopunks are one of
                the earliest examples of a &quot;Non-Fungible Token&quot; on
                Ethereum, and were inspiration for the ERC-721 standard that
                powers most digital art and collectibles.
              </p>
            </div>
          </div>
        </div>
      </section>
      {/* Hero Section End */}

      {/* Client Section Start */}
      <section id="clients" className="mt-12  w-full">
        <div className="w-full  h-full relative flex items-center justify-center overflow-hidden">
          <Image alt="image" src={nft} layout="intrinsic" objectFit="cover" />
        </div>
      </section>
      {/* Client Section End */}

      {/* Hero Section Start */}
      <section className="mt-12">
        <div className="container mx-auto">
          <div className="flex flex-col items-center justify-center flex-wrap">
            <div className="w-full self-center px-4 lg:w-3/4">
              <p className="font-medium text-secondary leading-relaxed text-lg  mt-4">
                The CryptoPunks are 10,000 uniquely generated characters. No two
                are exactly alike, and each one of them can be officially owned
                by a single person on the Ethereum blockchain. Originally, they
                could be claimed for free by anybody with an Ethereum wallet,
                but all 10,000 were quickly claimed. Now they must be purchased
                from someone via the marketplace that&apos;s also embedded in
                the blockchain. Via this market you can buy, bid on, and offer
                punks for sale. Below, you&apos;ll find information about the
                status of each Punk in the market. Punks with a blue background
                are not for sale and have no current bids. Punks with a red
                background are available for sale by their owner. Finally, punks
                with a purple background have an active bid on them.
              </p>
            </div>
            <Link href="/minting">
              <a className="px-8 py-3.5 bg-primary border border-primary rounded-full text-white hover:text-primary hover:bg-white transition ease-in-out duration-300 text-base font-semibold my-12">
                Minting
              </a>
            </Link>
          </div>
        </div>
      </section>
      {/* Hero Section End */}
    </div>
  );
}
